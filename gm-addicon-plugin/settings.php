<h2>Settings page</h2>
<form method="post" action="">
    
    <div>  
    <span>Active</span>
    <?php

        if(is_plugin_inactive('gm-addicon-plugin/gm_addicon_plugin.php')){
            echo "<h4>Plugin deactivated!!</h4>";
        }
        else{
            echo "<input type='submit' name ='deactivate' value='Disactive'>";
        }
    ?>    
    </div>
    <hr />   
    
       
    <h3>All posts</h3>
        
    <table>
        <tr>
            <th>Post title</th>
            <th>View</th>
            <th>Add icon</th>
        </tr>
        
        <?php 
                global $post;
                $allposts = get_posts();
                foreach($allposts as $post){
                setup_postdata($post);
        ?>
        <tr>
            <td><span><?php the_title(); ?></span></td>
            <td><a href="<?php the_permalink(); ?>"><span class="dashicons dashicons-visibility"></span></a></td>
            <td><a href="<?php $_SERVER['PHP_SELF'] ?>?page=my_plugin&c=gm_edit_title&id=<?php the_ID() ?>"><span class="dashicons dashicons-format-image"></span></a></td>
        </tr>
        <?php
        }
        wp_reset_postdata();
        ?>
    </table>    
</form>

