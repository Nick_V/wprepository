<?php
/*
Plugin Name: GM-AddIcon-Plugin
Plugin URI: http://wordpress.org
Description: Plugin that adds an icon to the title
Author: Mykola Hlushchenko
Version: 1.6
Author URI: https://bitbucket.org/Nick_V
*/



class GMActivate{
    
    function __construct() {
        add_action('admin_menu', array($this, 'gm_addsettings_menu'));
    }
    
    function gm_create_table(){
        global $wpdb;
        $table = $wpdb->prefix .'gm_table_icon';
        $sql = "CREATE TABLE $table(id int(11) NOT NULL AUTO_INCREMENT,post_id int(11) NOT NULL, icon_name varchar(255) NOT NULL, icon_position int(11) NOT NULL, PRIMARY KEY (id));"; 
        
        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        dbDelta($sql);
    }
    
    function gm_delete_table(){
        global $wpdb;
        $table = $wpdb->prefix .'gm_table_icon';
        $sql = "DROP TABLE IF EXISTS $table";
        $wpdb->query($sql);
    }
    
    function gm_save_settings($id, $icon, $pos){
        global $wpdb;
	$wpdb->insert('wp_gm_table_icon', array('post_id' => $id, 'icon_name' => $icon, 'icon_position' => $pos ),
		array('%d','%s','%d')
	); 
    }
    
    function gm_get_post_bytable($id){
        global $wpdb;
        $table = $wpdb->prefix .'gm_table_icon';
        $results = $wpdb->get_var($wpdb->prepare('SELECT * FROM '.$table.' WHERE post_id = %d', $id));
        return $results;
    }
    
    function gm_update_seetings_bypost($id, $new_icon,$new_position ){
        global $wpdb;
        $table = $wpdb->prefix .'gm_table_icon';
        $wpdb->update($table, array("icon_name" => $new_icon, "icon_position" => $new_position), array("post_id" => $id), array("%s", "%d"), array("%d") );
    }
    
    function  gm_get_all_settings(){
       global $wpdb;
       $table = $wpdb->prefix .'gm_table_icon'; 
       $settings = $wpdb->get_results("SELECT post_id, icon_name, icon_position FROM $table", ARRAY_N); 
       return $settings;
    }
    
    function gm_get_postId($title){
       global $wpdb;
       $table = $wpdb->prefix .'posts';
       $id = $wpdb->get_var("SELECT ID FROM $table WHERE post_title = '$title'");
       return $id;
    }
    
    function gm_get_iconName($array, $id){
        for($i = 0; $i < count($array); $i++){
            if($array[$i][0] == $id){
                return $array[$i][1];
            }
        }
    }
    
    function gm_get_iconPosition($array, $id){
        for($i = 0; $i < count($array); $i++){
            if($array[$i][0] == $id){
                return $array[$i][2];
            }
        }
    }
            
    function gm_addsettings_menu(){
        add_options_page('Mg_plugin', 'PostIcon', 8, 'my_plugin', array($this, 'render_gm_settings_page'));
      
    }
    
    function render_gm_settings_page(){
        switch ($_GET['c'])
            {
            case 'gm_edit_title': 
                $action = 'gm_edit_title';
                break;
            
            default :
                $action = 'settings';
                break;
            }
            include_once ("$action.php");
    } 
                
    function render_gm_select_icon(){
        include 'gm_edit_title.php';
    }
    
    function gm_deactivate_plugin(){
        add_action('admin_init', array($this, 'action_deactivate_plugin'));
    }
    
    function action_deactivate_plugin(){
        deactivate_plugins('gm-addicon-plugin/gm_addicon_plugin.php');
        $this->gm_delete_table();
    }
    
    function gm_add_icon_toposts(){
        add_filter('the_title', array($this, 'gm_insert_icon'));
    }
    
    function gm_insert_icon($title){
      
        $settings = $this->gm_get_all_settings();
        $id = $this->gm_get_postId($title);
        if($this->gm_get_post_bytable($id)){
           
            if($this->gm_get_iconPosition($settings, $id) == 1){
                $title = "<span class='dashicons {$this->gm_get_iconName($settings, $id)}'></span>". $title;    
            }else{
                $title = $title. "<span class='dashicons {$this->gm_get_iconName($settings, $id)}'></span>";     
            }
               
        }
        return $title;
    }
}

$gmplugin = new GMActivate();
$gmplugin->gm_create_table();
$gmplugin->gm_add_icon_toposts();

include 'gm_controller.php';



